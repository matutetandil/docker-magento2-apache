#########################################################################
#                                                               		#
# Apache Magento 2 Docker makefile                              		#
#                                                               		#
# Created by Matías Emanuel Denda Serrano matiasemanueldenda@gmail.com	#
#                                                               		#
# Usage:                                                        		#
#	make --> Install/start the Container                        		#
#	make up --> Same as make                                    		#
#	make down --> Stops containers and removes containers       		#
#	make stop --> Stops containers                              		#
#	make ssh --> Opens a terminal inside Magento container      		#
#	make restart --> Restart all the containers                 		#
#	make info --> Shows info about containers and proxy 				#
#	make hosts -->	Edits /etc/hosts with the hosts for the containers	#
#	make deploy	--> Runs the deploy script								#
#	make clean --> Destroy all images and configuration files   		#
#	make help --> Promt this message									#
#                                                               		#
#########################################################################

up: .env
	@sh tools/generate-containers.sh
	@sh tools/docker-info.sh
	@make proxy

down:
	@docker-compose down --remove-orphans

stop:
	@docker-compose stop

restart: stop up

clean: down
	@docker system prune -a --force
	@rm -f .env
	@rm -f .hosts
	@rm -f proxy/proxy-mac.pac

ssh:
	@sh tools/docker-exec.sh /bin/bash

.env:
	@sh tools/generate-config.sh

info:
	@sh tools/docker-info.sh

proxy/proxy-mac.pac:
	@sh tools/generate-proxy.sh

proxy: proxy/proxy-mac.pac

.hosts:
	@sh tools/generate-hosts.sh

hosts: .hosts

deploy:
	@sh tools/docker-exec.sh /root/deploy.sh

help:
	@echo "Usage:"
	@echo "	make		Install/start the Container"
	@echo "	make up		Same as make"
	@echo "	make down	Stops containers and removes containers"
	@echo "	make stop	Stops containers"
	@echo "	make ssh	Opens a terminal inside Magento container"
	@echo "	make restart	Restart all the containers"
	@echo "	make info	Shows info about containers and proxy"
	@echo "	make hosts	Edits /etc/hosts with the hosts for the containers"
	@echo "	make deploy	Runs the deploy script"
	@echo "	make clean	Destroy all images and configuration files"
	@echo "	make help	Promt this message"
