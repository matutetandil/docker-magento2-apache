#!/bin/bash

WEB_PATH='/var/www/html/magento2'

echo "Deploying code inside Web Container..."

cd $WEB_PATH
cd app/design/frontend/Venustheme/floristy_child/

npm start

cd $WEB_PATH

php bin/magento setup:upgrade
#php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy -t Venustheme/floristy_child
php bin/magento setup:static-content:deploy -t Venustheme/floristy_uk en_GB
php bin/magento setup:static-content:deploy -t Magento/backend
php bin/magento cache:clean
php bin/magento cache:flush

echo 'Deploy finished'
