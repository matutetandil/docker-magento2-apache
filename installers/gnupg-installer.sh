#!/bi/bash

pecl install gnupg
echo extension=gnupg.so | tee -a /etc/php/$1/mods-available/gnupg.ini
ln -s /etc/php/$1/mods-available/gnupg.ini /etc/php/$1/apache2/conf.d/10-gnupg.ini
ln -s /etc/php/$1/mods-available/gnupg.ini /etc/php/$1/cli/conf.d/10-gnupg.ini
service apache2 restart
