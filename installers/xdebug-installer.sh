#!/bin/bash

pecl install xdebug

sed -i '/display_errors = Off/c display_errors = On' /etc/php/$1/apache2/php.ini
sed -i '/error_reporting = E_ALL & ~E_DEPRECATED/c error_reporting = E_ALL | E_STRICT' /etc/php/$1/apache2/php.ini
sed -i '/html_errors = Off/c html_errors = On' /etc/php/$1/apache2/php.ini

sed -i '/display_errors = Off/c display_errors = On' /etc/php/$1/cli/php.ini
sed -i '/error_reporting = E_ALL & ~E_DEPRECATED/c error_reporting = E_ALL | E_STRICT' /etc/php/$1/cli/php.ini
sed -i '/html_errors = Off/c html_errors = On' /etc/php/$1/cli/php.ini

#DEFINITION OF THE VHOST CONF
XDEBUG_LOCATION=$(find / -name 'xdebug.so' 2> /dev/null)
XDEBUB_CONFIGURATION=$(cat <<EOF
[xdebug]
zend_extension='$XDEBUG_LOCATION'
;xdebug.remote_enable = 1
xdebug.remote_handler = dbgp
;xdebug.remote_host = 127.0.0.1
xdebug.client_host = 127.0.0.1
xdebug.idekey = PHPSTORM
;xdebug.remote_autostart = 1
xdebug.mode=debug
xdebug.start_with_request=yes
;xdebug.remote_connect_back = 1
xdebug.discover_client_host = 1
EOF
)

echo "$XDEBUB_CONFIGURATION" | tee -a /etc/php/$1/mods-available/xdebug.ini
ln -s /etc/php/$1/mods-available/xdebug.ini /etc/php/$1/apache2/conf.d/10-xdebug.ini
ln -s /etc/php/$1/mods-available/xdebug.ini /etc/php/$1/cli/conf.d/10-xdebug.ini
service apache2 restart
