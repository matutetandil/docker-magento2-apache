#!/bin/bash

echo "# Updating Repos..."
apt-get -qq update

echo "# Installing Tools..."
apt-get install -y -qq apt-utils \
                      vim \
                      less \
                      wget \
                      curl \
                      libgpgme11-dev \
                      mysql-client \
                      unzip \
                      cron \
                      git \
                      git-lfs \
                      > /dev/null 2>&1

echo "# Installing Apache..."
apt-get install -y -qq apache2 > /dev/null 2>&1
echo "ServerName localhost" >> /etc/apache2/apache2.conf
a2enmod rewrite > /dev/null 2>&1
a2enmod headers > /dev/null 2>&1
service apache2 restart

PHP_VERSION=''
PHP_DIR=''
case "$1" in
    7.3)
        echo "# Adding support for PHP 7.3"
        PHP_VERSION='7.3'
        PHP_DIR='7.3'
        apt-get install -y -qq software-properties-common
        add-apt-repository -y ppa:ondrej/php
        apt-get -qq update
        ;;
    *)
        echo "# Adding support for PHP 7.4"
        PHP_DIR="7.4"
        ;;
esac

echo "# Installing PHP..."
apt-get install -y -qq php$PHP_VERSION \
                      php$PHP_VERSION-bcmath \
                      php$PHP_VERSION-dom \
                      php$PHP_VERSION-intl \
                      php$PHP_VERSION-mbstring \
                      php$PHP_VERSION-zip \
                      php$PHP_VERSION-json \
                      php$PHP_VERSION-xml \
                      php$PHP_VERSION-soap \
                      php$PHP_VERSION-curl \
                      php$PHP_VERSION-gd \
                      php$PHP_VERSION-mysql \
                      php$PHP_VERSION-dev \
                      build-essential \
                      php-pear \
                      libpcre3-dev \
                      libmcrypt-dev \
                      gcc \
                      make \
                      autoconf \
                      libc-dev \
                      pkg-config \
                      supervisor \
                      > /dev/null 2>&1

echo "# Removing any PHP8 packages"
apt-get remove -y -qq php8.1-cli \
                    php8.1-common \
                    php8.1-opcache \
                    php8.1-readline\
                    > /dev/null 2>&1

echo "# Installing Extra Libraries"
yes '' | pecl install mcrypt-1.0.4 > /dev/null 2>&1
echo "extension=mcrypt.so" | tee -a /etc/php/$PHP_DIR/apache2/conf.d/20-mcrypt.ini
service apache2 restart

echo "# Installing NodeJS..."
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
nvm install 8.11.4
