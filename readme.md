# Description

This package contains a full Magento development environment. It was updated to run Magento 2.4.x.

The main difference between this environment and the others you can get on Internet, is that the containers DO NOT user your ports, they run independently with their own IP (like vagrant containers), this way, you can have several environments running at the same time without any kind of ports colissions.

It has xDebug already installed and configured for both apache and cli.
It also supports ARM environments like the new M1 Macs (please see the notes for Mac).

# Installation

## Enabling Docker Proxy (Olny for MacOS)

This will create a SOCKS5 Proxy on your localhost on port 8888, be sure you are not using it.

1. Install jq ```` brew install jq ````
2. Stop Docker Desktop
3. ```` cd ~/Library/Group\ Containers/group.com.docker/ ````
4. ```` mv settings.json settings.json.backup ````
5. ```` cat settings.json.backup | jq '.["socksProxyPort"]=8888' > settings.json ````
6. Start Docker Desktop

## Running the containers

In order to start using the containers do this:

1. Create a 'code' directory and clone your Magento code there.
2. Create a 'db/dump' directory and put your sql dump there. This dump will be automatically populated to the database during the first run. (Check MariaDB Docker documentation for more details)
3. Create an 'assets' directory and put there any file you want to be inside the Magento container
4. run ````make```` and answer the questions.
5. Once it finishes, configure the Docker Proxy (if you are in a mac) in your local environment.
6. Add an entry to your localhost for the WEB container, for instance:

````
172.20.0.100 develop.magento.com
172.20.0.100 www.develop.magento.com
````
## Configuring database in sequel Pro or similar
1. Create a new connection using the IP provided
2. User and Password are the ones you set in the first step of the make command.
3. You can now import the database.

## Using Magento

Once you have running the containers and the database is imported, you will be able to access to 'develop.magento.com' (if that was the domain configured). To configure Magento use 'db' or 'root_db_1' as database server, and the username, password, and database you provide during the configuration.

## Debuging

The web container has XDebug already installed.

## Redis

A Redis container is also provided. In order to use it, just use 'redis' or 'root_redis_1' as redis server. The port is redis default port.

## ElasticSearch

An ElasticSearch (7.9.3) container is also provided. In order to use it, just use 'elasticsearch' or 'root_elasticsearch_1' as elasticsearch server. The port is ElasticSearch default port.

## Configuring Docker Proxy

After running make, you will be serving a very little nginx client on port 8000 (You can change it during configuration on the first run). This is used to configure your proxy network using a PAC file.
This will allow you to access the Docker Containers using theirs IP, so there is no need to stop your local services, they will not be in conflict with docker ones.

### Global configuration
1. Go to System Preferences -> Network -> Advanced -> Proxies
- Select Automatic Proxy configuration
- Enter 'http://localhost:8000/proxy-mac.pac'
- Apply changes and save configuration.

# Directory Mapping
- code --> /var/www/html/magento2
- assets --> /root/assets
- db/dump --> /docker-entrypoint-initdb.d

# Possible Errors
If you get an error while running ````npm install````:

ajv-keywords@2.1.1 requires a peer of ajv@^5.0.0 but none is installed. You must install peer dependencies yourself.

````npm install ajv@^5.0.0 --save````


If you get an error running ````npm start```` regarding gulp:

````npm install gulp````

# XDebug extra configuration in MacOS

Please follow this guide, but keep port 9000 for xdebug: https://gist.github.com/manuelselbach/8a214ae012964b1d49d9fb019f5f5d7b

Also, you need to change this options in xdebug.ini (/etc/php/<PHP_VERSION>/apache2/conf.d/10-xdebug.ini) file.

PHP_VERSION is 7.4 or 7.3 (depends and what you've chosen)

````
xdebug.remote_host = 10.254.254.254
xdebug.remote_connect_back = 0
````
For newer versions of Xdebug

````
xdebug.client_host = 10.254.254.254
xdebug.discover_client_host = 0
````