# Dockerfile for Magento 2 Docker Apache
FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive

ARG PHP_VERSION

ADD installers/system-tools-installer.sh /tmp/system-tools-installer.sh
RUN sh /tmp/system-tools-installer.sh $PHP_VERSION

RUN echo "# Installing XDebug..."
ADD installers/xdebug-installer.sh /tmp/xdebug-installer.sh
RUN sh /tmp/xdebug-installer.sh $PHP_VERSION > /dev/null 2>&1

COPY installers/000-default.conf /etc/apache2/sites-available/000-default.conf
RUN service apache2 restart

RUN echo "# Installing GNUPG..."
ADD installers/gnupg-installer.sh /tmp/gnupg-installer.sh
RUN sh /tmp/gnupg-installer.sh $PHP_VERSION > /dev/null 2>&1

RUN echo "# Installing Composer..."
ADD installers/composer-installer.sh /tmp/composer-installer.sh
RUN sh /tmp/composer-installer.sh > /dev/null 2>&1

# RUN echo "# Installing Prestissimo..."
# run composer global require hirak/prestissimo

RUN echo "# Installing PHP Unit..."
ADD installers/phpunit-installer.sh /tmp/phpunit-installer.sh
RUN sh /tmp/phpunit-installer.sh > /dev/null 2>&1

RUN echo "# Installing n98 Magerun 2..."
ADD installers/n98-magerun2-installer.sh /tmp/n98-magerun2-installer.sh
RUN sh /tmp/n98-magerun2-installer.sh > /dev/null 2>&1

RUN echo "#Configuring Supervisor..."
RUN mkdir -p /var/lock/apache2 /var/run/apache2 /var/run/sshd /var/log/supervisor

COPY installers/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2

RUN echo "#Copying deploy script"
COPY installers/deploy.sh /root

EXPOSE 80

CMD ["/usr/bin/supervisord"]
