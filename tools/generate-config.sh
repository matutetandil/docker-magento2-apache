#!/bin/bash

read -r -p "Enter your PHP Version [7.3]: " PHP; echo PHP_VERSION=${PHP:-7.3} > .env
read -r -p "Enter your domain name [waterworks.com]: " DOMAIN; echo DOCKER_DOMAIN=${DOMAIN:-waterworks.com} >> .env
read -r -p "Enter MySQL Root Password [root]: " ROOTPASS; echo MYSQL_ROOT_PASSWORD=${ROOTPASS:-root} >> .env
read -r -p "Enter MySQL Database [waterworks]: " DB; echo MYSQL_DATABASE=${DB:-waterworks} >> .env
read -r -p "Enter MySQL User [waterworks]: " USER; echo MYSQL_USER=${USER:-waterworks} >> .env
read -r -p "Enter MySQL User Password [waterworks]: " PASS; echo MYSQL_PASSWORD=${PASS:-waterworks} >> .env

if [ $(uname -s) == "Darwin" ]; then
    read -r -p "Enter Port for Internal Proxy Server [8000]: " PORT; echo NGINX_INTERNAL_PORT=${PORT:-8000} >> .env
    read -r -p "Enter Port of Docker SOCKS Proxy [8888]: " SOCKS; echo DOCKER_SOCKS_PORT=${SOCKS:-8888} >> .env
fi

echo PHP_IDE_CONFIG="serverName=localhost" >> .env

echo WEB_CONTAINER_IP="172.20.0.100" >> .env
echo DB_CONTAINER_IP="172.20.0.101" >> .env
echo PHPMYADMIN_CONTAINER_IP="172.20.0.102" >> .env
echo ES_CONTAINER_IP="172.20.0.103" >> .env
echo REDIS_CONTAINER_IP="172.20.0.104" >> .env
