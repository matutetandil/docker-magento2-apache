#!/bin/bash

echo "Docker Proxy is no needed any more"
exit 0;

source .env

PROXY_CONFIGURATION=$(cat <<EOF
function FindProxyForURL(url, host) {
    if (isInNet(host, "172.0.0.0", "255.0.0.0")) {
        return "SOCKS5 localhost:$DOCKER_SOCKS_PORT";
    } else {
        return "DIRECT";
    }
}
EOF
)

echo "$PROXY_CONFIGURATION" > proxy/proxy-mac.pac
