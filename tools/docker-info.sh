#/bin/bash

source .env

CONTAINER_MAGENTO=$(docker ps | grep _magento | rev | cut -d' ' -f1 | rev)
CONTAINER_MARIADB=$(docker ps | grep _db | rev | cut -d' ' -f1 | rev)
CONTAINER_PHPMYADMIN=$(docker ps | grep _phpmyadmin | rev | cut -d' ' -f1 | rev)


echo ""
echo "--------------------------------------------------------------------------------"
CONTAINER_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $CONTAINER_MAGENTO)
echo "Your WEB Container IP is: $CONTAINER_IP"

echo ""
CONTAINER_DB_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $CONTAINER_MARIADB)
echo "Your DB Container IP is: $CONTAINER_DB_IP"

echo ""
CONTAINER_PHPMYADMIN_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $CONTAINER_PHPMYADMIN)
echo "Your PhpMyAdmin Container IP is: $CONTAINER_PHPMYADMIN_IP"

if [ $(uname -s) == "Darwin" ]; then
    echo ""
    CONTAINER_NGINX=$(docker ps | grep _proxy | rev | cut -d' ' -f1 | rev)
    NGINX_PROXY_PORT=$(docker inspect --format='{{range $p, $conf := .NetworkSettings.Ports}} {{$p}} -> {{(index $conf 0).HostPort}} {{end}}' $CONTAINER_NGINX | cut -d '>' -f2 | awk '{$1=$1};1')
    echo "Your PAC file URL is: http://localhost:$NGINX_PROXY_PORT/proxy-mac.pac"

    echo ""
    echo "Remember to configure you system proxy to use the pac file provided."
    echo "Read the readme to get more information."
fi
echo ""
echo "run 'make ssh' to connect to your WEB container"
echo "--------------------------------------------------------------------------------"
echo ""
echo "Please, run 'make hosts' as root in order to create the following hosts entries:"
echo ""
echo  "$CONTAINER_IP www.develop.$DOCKER_DOMAIN"
echo  "$CONTAINER_IP develop.$DOCKER_DOMAIN"
echo  "$CONTAINER_DB_IP db.$DOCKER_DOMAIN"
echo  "$CONTAINER_PHPMYADMIN_IP phpmyadmin.$DOCKER_DOMAIN"

echo ""
