#!/bin/bash

if [[ $(id -u) -ne 0 ]] ; then
    echo "Please run as root" ; exit 1 ;
fi

source .env

CONTAINER_MAGENTO=$(docker ps | grep _magento | rev | cut -d' ' -f1 | rev)
CONTAINER_MARIADB=$(docker ps | grep _db | rev | cut -d' ' -f1 | rev)
CONTAINER_PHPMYADMIN=$(docker ps | grep _phpmyadmin | rev | cut -d' ' -f1 | rev)

CONTAINER_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $CONTAINER_MAGENTO)
CONTAINER_DB_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $CONTAINER_MARIADB)
CONTAINER_PHPMYADMIN_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $CONTAINER_PHPMYADMIN)

HOSTS=$(cat <<EOF

############### DOCKER MAGENTO 2 HOSTS ###############
$CONTAINER_IP    develop.$DOCKER_DOMAIN
$CONTAINER_IP    www.develop.$DOCKER_DOMAIN

$CONTAINER_DB_IP    db.$DOCKER_DOMAIN

$CONTAINER_PHPMYADMIN_IP    phpmyadmin.$DOCKER_DOMAIN
############### DOCKER MAGENTO 2 HOSTS ###############
EOF
)

echo "$HOSTS" >> /etc/hosts

touch .hosts
