#!/bin/bash

SUFIX=''

if [ $(uname -s) == "Darwin" ]; then
    SUFIX='.mac'
fi

FILE="docker-compose$SUFIX.yml"


docker-compose -f $FILE up -d
