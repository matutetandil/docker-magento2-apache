#!/bin/bash

CONTAINER_MAGENTO=$(docker ps | grep _magento | rev | cut -d' ' -f1 | rev)

docker exec -it $CONTAINER_MAGENTO $1
